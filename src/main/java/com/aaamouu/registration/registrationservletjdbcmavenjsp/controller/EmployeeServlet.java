package com.aaamouu.registration.registrationservletjdbcmavenjsp.controller;

import com.aaamouu.registration.registrationservletjdbcmavenjsp.dao.EmployeeDao;
import com.aaamouu.registration.registrationservletjdbcmavenjsp.model.Employee;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "EmployeeServlet", value = "/register")
public class EmployeeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;


    //Using the DAO
    private EmployeeDao employeeDao = new EmployeeDao();

    public EmployeeServlet() {
        super();
    }



    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/WEB-INF/views/employeeRegister.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String address = request.getParameter("address");
        String contact = request.getParameter("contact");

        Employee employee = new Employee();

        employee.setFirstname(firstname);
        employee.setLastname(lastname);
        employee.setUsername(username);
        employee.setPassword(password);
        employee.setAddress(address);
        employee.setContact(contact);

        try {
            employeeDao.registerEmployee(employee);
        } catch (ClassNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //Page de Redirection
        request.getRequestDispatcher("/WEB-INF/views/employeeDetails.jsp").forward(request, response);

    }
}
